import * as homePage from '../../pageObjects/homepage.page'
import * as searchPage from '../../pageObjects/searchPage.page'
import * as itemDetails from '../../pageObjects/itemDetails.page'
import * as myBag from '../../pageObjects/myBag.page'
const assert = require('assert');

const steps = function () {

	let itemName;
    this.When(/^I am on the landing page$/, () => {
        homePage.goToPage()
    });

    this.Then(/^I can see logo in navigation bar$/, () => {
        assert.ok(homePage.hasLogo())
    });
    
    this.When(/^I search for a random product$/, () => {
		homePage.openSearch()
		homePage.searchForAnItem("trousers")
		homePage.clickBrowseButton()
	});
	
	this.Then(/^the product listing page should display a list of products$/, () => {
		assert.ok(searchPage.hasItemsList())
	});
	
	this.When(/^I select a random product from the returned list$/, () => {
		searchPage.selectRandomItem()
	});
	
	this.Then(/^I should be taken to the product detail page$/, () => {
		assert.ok(itemDetails.areDetailsVisible())
	});
	
	this.When(/^I add the selected product to the basket$/, () => {
		 const isDropdownVisible = itemDetails.isDropdownVisible()
		 if(isDropdownVisible){
			 itemDetails.clickSizesDropdown()
			 itemDetails.selectFirstSizeAvailable()
		 }
		 else{
			 itemDetails.clickSizeNoDropdown()
		 }
		 itemName = itemDetails.getItemName()
		 itemDetails.addToBag()
	});
	
	this.When(/^I change the product quantity$/, () => {
		itemDetails.clickViewBagButton()
		myBag.clickEditButton()
		myBag.changeQuantity('2')
		myBag.saveChanges()
	});
	
	this.Then(/^I should see the product added to the basket/, () => {
		assert.ok(myBag.isCorrectItem(itemName))
	});
	
	this.Then(/^I should see the selected quantity for the product/, () => {
		assert.ok(myBag.isCorrectQuantity('2'))
	});
	
	this.When(/^I open the basket and remove the added product$/, () => {
		itemDetails.clickViewBagButton()
		myBag.removeItem()
		myBag.closeModal()
	});
	
	this.Then(/^my basket should be empty/, () => {
		assert.ok(myBag.isEmptyBasket())
	});

};

module.exports = steps;