const brandLogo = '.BrandLogo-img'
const headerSearchIcon = '.Header-searchButton'
const searchForm = '.SearchBar-form'


export const goToPage = () => browser.url('/')
export const hasLogo = () => browser.isVisible(brandLogo)
export const openSearch = () => browser.click(headerSearchIcon)
export const searchForAnItem = itemToSearch => browser.keys(itemToSearch)
export const clickBrowseButton = () => browser.submitForm(searchForm)