const productDetail = '.ProductDetail'
const productSizesDropdown = '#productSizes'
const firstSizeEnabled = '.Select-select--option:enabled'
const addToBagButton = '.Button.AddToBag'
const shownModal = '.Modal.is-shown'
const viewBagButton = '.Button.AddToBagConfirm-viewBag'
const itemName = '.ProductDetail-title'
const productSizeButton = '.ProductSizes-list > button'
	
export const areDetailsVisible = () => browser.waitForVisible(productDetail, 5000)
export const isDropdownVisible = () => browser.isVisible(productSizesDropdown)
export const clickSizesDropdown = () => {
	browser.scroll(productSizesDropdown)
	browser.click(productSizesDropdown)
}
export const clickSizeNoDropdown = () => browser.click(productSizeButton)
export const selectFirstSizeAvailable = () => browser.click(firstSizeEnabled)
export const addToBag = () => {
	browser.scroll(addToBagButton)
	browser.click(addToBagButton)
}

export const clickViewBagButton = () =>{
	browser.waitForVisible(shownModal, 5000)
	browser.click(viewBagButton)
}
export const getItemName = () => browser.getText(itemName)