const editButton = '.OrderProducts-editText'
const quantityDropdown = '#bagItemQuantity'
const loaderOverlay = '.LoaderOverlay'
const saveButton = '.Button.OrderProducts-saveButton'
const productName = '.OrderProducts-productName'
const productLabel = '.OrderProducts-row.OrderProducts-productSize .OrderProducts-label'
const removeButton = '.OrderProducts-deleteText'
const modal = '.Modal.is-shown'
const deleteButton = '.Button.OrderProducts-deleteButton'
const okButtonModal = '.OrderProducts-modal > button'
const emptyBasketLabel = '.MiniBag-emptyLabel'
const emptyBasketText = 'Your shopping bag is currently empty.'
	
export const clickEditButton = () =>{
	browser.waitForVisible(editButton)
	browser.click(editButton)
}

export const changeQuantity = (quantity) =>  {
	browser.waitForVisible(loaderOverlay, 5000, true)
	browser.selectByValue(quantityDropdown, quantity)
}

export const saveChanges = () => browser.click(saveButton)
export const isCorrectItem = (expectedName) => {
	let currentName = browser.getText(productName)
	return currentName == expectedName
}
export const isCorrectQuantity = (expectedQuantity) => {
	browser.waitForVisible(loaderOverlay, 5000, true)
	let productLabelText = browser.getText(productLabel)
	let currentQuantity = productLabelText.substring(0,1)
	return currentQuantity == expectedQuantity
}

export const removeItem = () => {
	browser.waitForVisible(removeButton, 3000)
	browser.click(removeButton)
	browser.waitForVisible(modal, 3000)
	browser.click(deleteButton)
}

export const closeModal = () => {
	browser.waitForVisible(modal, 3000)
	browser.click(okButtonModal)
}

export const isEmptyBasket = () => { 
	let emptyBagText = browser.getText(emptyBasketLabel)
	return emptyBagText == emptyBasketText
}